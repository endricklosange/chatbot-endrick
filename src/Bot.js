const Bot = class Bot {
  constructor(name, image) {
    this.name = name;
    this.image = image;
    this.date = new Date();
  }

  botHello() {
    return `
    <div class="received col-6 mt-5">
          <div class="card text-left">
            <div class="card-body my-2">
              <div class="row">
                <div class="col-2">
                  <img src="${this.image}" class="img-fluid" alt="image user" width="100px">
                  <p class="card-text">${this.name}</p>
                </div>
                <div class="col-10">
                  <p class="card-text">Salut endrick</p>
                </div>
              </div>
            </div>
            <p class="d-flex justify-content-end mx-2">${this.date.toDateString()}</p>
          </div>
    </div>
    <div class="col-6"></div>
    `;
  }

  botJoke() {
    const url = fetch("https://api.chucknorris.io/jokes/random")
      .then((response) => response.json())
      .then((data) => {
        return data.value;
      });

    const printUrl = async () => {
      const a = await url;
      return a;
    };
    return `
    <div class="received col-6 mt-5">
      <div class="card text-left">
        <div class="card-body my-2">
          <div class="row">
            <div class="col-2">
              <img src="${this.image}" class="img-fluid" alt="image user" width="100px">
              <p class="card-text">${this.name}</p>
            </div>
            <div class="col-10">
              <p class="card-text">${printUrl()}</p>
            </div>
          </div>
        </div>
        <p class="d-flex justify-content-end mx-2">${this.date.toDateString()}</p>
      </div>
    </div>
    <div class="col-6"></div>`
  }
  botHelp() {
    return `
  <div class="received col-6 mt-5">
        <div class="card text-left">
          <div class="card-body my-2">
            <div class="row">
              <div class="col-2">
                <img src="${this.image}" class="img-fluid" alt="image user" width="100px">
                <p class="card-text">${this.name}</p>
              </div>
              <div class="col-10">
                <p class="card-text">tu peux tapper dans le chat bot les commandes suivante "Salut ,Une blague"</p>
              </div>
            </div>
          </div>
          <p class="d-flex justify-content-end mx-2">${this.date.toDateString()}</p>
        </div>
  </div>
  <div class="col-6"></div>
  `;
  }
  botCook() {
    const url = fetch("www.themealdb.com/api/json/v1/1/random.php")
      .then((response) => response.json())
      .then((data) => {
        return data;
      });

    const printUrl = async () => {
      const a = await url;
      console.log(a);

    };
    return `
 <div class="received col-6 mt-5">
   <div class="card text-left">
     <div class="card-body my-2">
       <div class="row">
         <div class="col-2">
           <img src="${this.image}" class="img-fluid" alt="image user" width="100px">
           <p class="card-text">${this.name}</p>
         </div>
         <div class="col-10">
           <p class="card-text">${printUrl()}</p>
         </div>
       </div>
     </div>
     <p class="d-flex justify-content-end mx-2">${this.date.toDateString()}</p>
   </div>
 </div>
 <div class="col-6"></div>`
  }

};
export default Bot;
