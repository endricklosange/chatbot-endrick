import Bot from "./Bot";

const Chat = class Chat {
  constructor() {
    this.el = document.querySelector('#app');
    this.bot1 = new Bot('meteo', 'http://media3.woopic.com/api/v1/images/156%2Fv%2FIks1O1W1jPQ3zbwfe%2Fyanet-garcia-miss-meteo-mexique%7Cx240?format=470x264&facedetect=1&quality=85');
    this.bot2 = new Bot('Philippe Etbottest', 'https://www.atabula.com/wp-content/uploads/2019/02/Philippe-Etchebest.png');
    this.bot3 = new Bot('funny', 'https://static.hitek.fr/img/up_m/1423379912/elrisitas.webp');
  }

  renderUser(user) {
    return `
      <div class="row my-2">
        <div class="col-6 ">
          <img src="${user.image}" class="img-fluid" alt="image user">
        </div>
        <div class="col-4">
          <h3 class="pt-5 h1 mt-3">${user.name}</h3>
        </div>
      </div>
  `;
  }

  renderUsers(users) {
    return `
        <div class="col-4 bg-primary">
          ${users.map((user) => this.renderUser(user)).join('')}
        </div>
        `;
  }
  renderMessageSend(message) {
    const date = new Date();
    return `
    <div class="col-6"></div>
    <div class="received col-6 mt-5">
          <div class="card text-left">
            <div class="card-body my-2">
              <div class="row">
                <div class="col-2">
                  <img src="${message.userImage}" class="img-fluid" alt="image user" width="100px">
                  <p class="card-text">Moi</p>
                </div>
                <div class="col-10">
                  <p class="card-text">${message.text}</p>
                </div>
              </div>
            </div>
            <p class="d-flex justify-content-end mx-2">${date.toDateString()}</p>
          </div>
    </div>
    `;
  }
  renderMessageReceived(message) {
    const messagesEl = document.querySelector('.messages');

    if (message === "Salut") {
      messagesEl.innerHTML += this.bot1.botHello();
      messagesEl.innerHTML += this.bot2.botHello();
      messagesEl.innerHTML += this.bot3.botHello();
    }
    if (message === "Une blague") {
      messagesEl.innerHTML += this.bot3.botJoke();
    }
    if (message === "help") {
      messagesEl.innerHTML += this.bot1.botHelp();
    }

  }
  renderMessages() {
    return `
    <div class="col-8 bg-secondary position-relative">
      <div class="row messages">

      </div>
    </div>`;
  }

  renderForm() {
    return `
        <div class="spacer"></div>
        <div class="row position-absolute fixed-bottom m-1 typing-message">
          <div class="col-8">
            <input id="input" class="form-control" type="text" placeholder="Default input" aria-label="default input example">
          </div>
          <div class="col-4 d-grid gap-2">
            <button type="button" class="btn btn-primary gap-2">Primary</button>
          </div>
        </div>
        `;
  }
  typingMessage() {
    const el = document.querySelector('.typing-message input');
    const btnEl = document.querySelector('.typing-message button');

    const messagesEl = document.querySelector('.messages');

    el.addEventListener('keypress', (e) => {
      if (e.keyCode === 13) {
        const text = e.currentTarget.value;
        const message = {
          userImage: "https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png",
          text
        }
        this.renderMessageReceived(text)
        messagesEl.innerHTML += this.renderMessageSend(message);
        e.currentTarget.value = "";
      }
    });
    btnEl.addEventListener('click', (e) => {
      const text = el.value;
      const message = {
        userImage: "https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png",
        text
      }
      this.renderMessageReceived(text)
      messagesEl.innerHTML += this.renderMessageSend(message);
      el.value = "";
    });

  }
  render() {
    const users = [
      {
        name: this.bot1.name,
        image: this.bot1.image
      },
      {
        name: this.bot2.name,
        image: this.bot2.image
      },
      {
        name: this.bot3.name,
        image: this.bot3.image
      }
    ];
    return `
        <div class="container-fluid">
            <div class="row">
            ${this.renderUsers(users)}
            ${this.renderMessages()}
            ${this.renderForm()}
            </div>
        </div>
        `;
  }
  run() {
    this.el.innerHTML += this.render();
    this.typingMessage();
  }
};
export default Chat;
