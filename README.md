# Hello World

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v14 use :
```bash
nvm install 14
```

```bash
npm i
```

## Usage

Name
Chatbot Endrick

Description
Réalisation de l'évaluation Chatbot Endrick


Installation
1: Pour lancer le serveur lancer npm run start


2: Lancer la commande help pour voir la liste des fonctionnalité